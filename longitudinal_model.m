%% Longitudinal model

%% Coefficents 
init
[Wing,VOP,Plane,Trim] = Design_X57();

rho = 1.225;                % air density
g = 9.81;           

% VOP is rear wing
% Wing is front wing
m = Plane.MTOW;             % Aircraft weight         
J_y = Plane.J(2,2);         % Aircraft inertia
Aw1 = VOP.cW.*VOP.dP;       % Area of wing elements corresponding to VOP in notation from Design_X57
Aw2 = Wing.cW.*Wing.dP;     % Area of wing elements corresponding to Wing in notation from Design_X57

Ap1 = pi.*(VOP.dP.^2)/4;    % Propeller disk area of engines mounted in VOP
Ap2 = pi.*(Wing.dP.^2)/4;   % Propeller disk area of engines mounted in Wing

d1 = VOP.dx;                % Distance of VOP in x axis from CG
d2 = Wing.dx;               % Distance of Wing in x axis from CG

z1 = VOP.dz;                % Distance of VOP in z axis from CG
z2 = Wing.dz;               % Distance of Wing in z axis from CG

C_Lalpha0 = Wing.CL;        % Lift coefficient at zero angle of attack
C_Lalpha = pi*2;            % Lift coefficient   
C_Dalpha = 0.01;            % Drag coefficient at zero angle of attack
C_Dalpha0 = C_Lalpha0/15;   % Drag coefficient   


%% Vectorization
Aw1 = [Aw1 Aw1(end:-1:1)];                      % Mirror of front engines wing area
Aw2 = [Aw2 Aw2(end:-1:1)];                      % Mirror of rear engines wing area
Aw = [Aw1 Aw2];                                 % Array of wing area for front and rear engines

Ap1 = [Ap1, Ap1(end:-1:1)];                     % Mirror of front engines proppeler disk area
Ap2 = [Ap2, Ap2(end:-1:1)];                     % Mirror of rear engines proppeler disk area
Ap = [Ap1 Ap2];                                 % Array of propeller disk area for front and rear engines 

d = [d1*ones(size(Ap1)) d2*ones(size(Ap2))]*1;  % Array contains distance from CG in x axis for each engine
z = [z1*ones(size(Ap1)) z2*ones(size(Ap2))]*1;  % Array contains distance from CG in z axis for each engine

v_cruise = Trim.v_cruise;                       % Cruise speed

T_0 = [Trim.VOPThrust*ones(size(Ap1)) Trim.WingThrust*ones(size(Ap2))]; % Thrust command for each engine at cruise speed

num_engines = length(Aw);                       % Number of engines

%% Nonlinear equations (vectorized)
ve = @(T,X) sqrt(2.*T./(rho.*Ap)+(X(1).^2));                                            % Induced airspeed based on thrust: T = 0.5*rho*Ap*(ve^2-v0^2)
alpha = @(X,v_e) atan((X(2)-X(3)*d)./v_e);                                              % Angle of attack (vz speed is influenced by the pitch rate)
L = @(T,X) 0.5*rho.*Aw.*(C_Lalpha.*(alpha(X,ve(T,X))+X(4))+C_Lalpha0).*(ve(T,X)).^2;    % Vector containing generated wind lift in body coordinate system
D = @(T,X) 0.5*rho.*Aw.*(C_Dalpha.*(alpha(X,ve(T,X))+X(4))+C_Dalpha0).*(ve(T,X)).^2;    % Vector containing generated wind drag in body coordinate system
Fx = @(T,X) T*cos(X(4))-D(T,X).*cos(alpha(X,ve(T,X)))+L(T,X).*sin(alpha(X,ve(T,X)));    % Vector contains forces acting in x axis in body coordinate system
Fz = @(T,X) -T*sin(X(4))-L(T,X).*cos(alpha(X,ve(T,X)))-D(T,X).*sin(alpha(X,ve(T,X)));   % Vector contains forces acting in z axis in body coordinate system

% Nonlinear differential eqauation containing state derivation
% State X = [vx, vz, dtheta, theta]
%           [horizontal speed, vertical speed, pitch rate, pitch angle]
diff_X= @(T,X) [sum(Fx(T,X))/m-g*sin(X(4));...
                (sum(Fz(T,X)))/m+g*cos(X(4));...
                sum(z.*Fx(T,X)-d.*Fz(T,X))./J_y;...
                X(3)];

%% Finding the trimming point
X_s = sym('X_', [1 4]);             % Symbolic variables for states
T_s = sym('T_', [1 num_engines]);   % Symbolic variables for engines thrust

X_0 = [v_cruise 0 0 0];

f_opt =@(T) norm(diff_X(T(1:(end-1)), [X_0(1:end-1),T(end)])) ; % Function for optimization giving norm of state derivatives 
%% Linear contraints on engines thrust for finding the trimming point

%Thrust is symetrically distributed for engines along wing. 
Aeq = [throttle_constraint_matrix(length(Ap1)), zeros(length(Ap1),length(Ap2));...
    zeros(length(Ap2),length(Ap1)), throttle_constraint_matrix(length(Ap2))];
Aeq = [Aeq,zeros(length(Aeq),1)];
Beq = zeros(size(Ap))';

%% Optimization
Y = [T_0,0]; % We optimize the thrust of engines and aircraft pitch angle in order to trim the aircraft
[Y,f_val]=fmincon(f_opt,Y,[-eye(length(Aw)),zeros(length(Aw),1)],zeros(size(Ap))',Aeq,zeros(size(Ap))'); % Optimization. Output is optimized thrust T_0 and value of minimalized function.
theta_0 = Y(end);
T_0 = Y(1:(end-1)); % Trim thrust
X_0(end) = theta_0;
%% Linearization
A = jacobian(diff_X(T_s,X_s),X_s);
B = jacobian(diff_X(T_s,X_s),T_s);
A = double(subs(A,[X_s T_s],[X_0 T_0])); % Matrix A of linerized system
B = double(subs(B,[X_s T_s],[X_0 T_0])); % Matrix B of linerized system

%% Augmented system
X_0 = [X_0 1000];                   % Trimming point
A_long = [A, zeros(4,1);[0 -1 0 0 0]]; % Added altitude state
B_long = [B;zeros(1,length(B))];       % Added altitude state

%% Functions

% Function computes matrix, in which are contraints for distribution of thrust encoded. 
function [A]=throttle_constraint_matrix(dim)
    A= zeros(dim);
    one_pos = 1;
    onem_pos = dim;
    for i = 1:dim
       if one_pos > onem_pos
           break
       end
       A(i,[one_pos, onem_pos]) = [1 -1];
       one_pos = one_pos+1;
       onem_pos = onem_pos -1;
    end
end
