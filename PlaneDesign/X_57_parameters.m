global dP_w cW_w dP_v cW_v rho CL_cruise MTOW J
%% Plane parameters
MTOW = 1230;        % based on (identical to) Tecnam P2006T aircraft MTOW
J = diag([4/3,1,9])*MTOW/2;%0.1423;  % complete guess, sadly
CL_cruise = 0.3;    % Estimated CL in cruise for Tecnam P2006T aircraft (source: Wing conceptual design for the airplane with
% distributed electric propulsion od Pavel Hospodáč... Tecnam P2006T
% aircraft CL in cruise is 0.23...but 0.3 gives lower cruise speed (and
% probably more realistic)
%% Wing parameters
A_w = 7.4;         % Wing area...simply Tecnam P2006T wing area /2
c1_w = 0.25;       % Wing geometry...Viz OPENVSP (used only for vizualization)
AR_w = 17.6;       % Wing aspect ratio... simply Tecnam P2006T AR*2
dP = [1,ones(1,6).*1/sqrt(6)]*1; % Ratio between engines...Wing tip engine has the same disk are as the remaining "HIGH LIFT" engines
BR_w = 1.22/2;     % Fuselage ratius at the wing...Estiamted from Tecnam P2006T drawing
MCR_w = 0.25;      % Minimal cord thickness compared to it's mean value
[dP_w,cW_w,cWW_w,rP_w,rW_w]=WingDesign(A_w,AR_w,CL_cruise,J(1,1),J(3,3),dP,BR_w,MCR_w);
%% VOP parameters
A_v = 1.2159;      % VOP area...estimated from Tecnam P2006T drawing
c1_v = 0.25;       % Wing geometry...Viz OPENVSP (used only for vizualization)
AR_v = 17.6;       % VOP aspect ratio... same as wing
dP = [0.1,0.1,0.1];    % Engines have the same size
BR_v = 0.5/2;      % Fuselage ratius at the VOP...Estiamted from Tecnam P2006T drawing
MCR_v = 0.5;       % Minimal cord thickness compared to it's mean value
[dP_v,cW_v,cWW_v,rP_v,rW_v]=WingDesign(A_v,AR_v,CL_cruise,J(1,1),J(3,3),dP,BR_v,MCR_v);
%% Mission parameters
rho = 1.225;       % Air density at sea level [kg*m^3]
g = 9.81;          % gravity acceleration at sea level

