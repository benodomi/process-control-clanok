function [dP_out,cW_out,cWW2,rP,rW]=WingDesign(WA,AR,CL_v,J_x,J_z,dP_in,BR,MCR)
global dP
dP = dP_in;
cW_i = [1,1,1,1];
global nAgents
nAgents = length(dP)*2;
global Jrp
Jrp = diag([J_x,J_z]);
global CL
CL = CL_v;
global AspectRatioReq
AspectRatioReq = AR;
global WAReq
WAReq = WA;
global BodyRadius
BodyRadius = BR;
global minWingCord
minWingCord = sqrt(WAReq/AspectRatioReq)*MCR;


x0 = [1,cW_i];
options = optimoptions('fmincon','Display','none','Algorithm','sqp','MaxFunctionEvaluations',1e6);

x = fmincon(@cost,x0,[],[],[],[],[],[],@mycon,options);

[dP_out,cW_out,cWW2,rP,rW] = coefs2var(x);


[dP_out,cW_out,cWW2,rP,rW];
end
function [dPP,cW,cWW,rP,rW] = coefs2var(x)
global dP
global nAgents
dPP = dP;
cW_i = x(2:end);
coefs = linspace(0,sum(dPP)-dPP(1)/2,nAgents);
cWW = polyval(cW_i,coefs);
cW = ones(size(dPP));
rP = [cumsum(dPP,'reverse'),-0.1];
rW = linspace(max(rP)-dPP(1)/2,0,length(cWW));
for i = 1:length(cW)
    cW(i) = mean(cWW(rW<=rP(i)&rW>rP(i+1)));
end
cW(1) = cW(1)/2;
dPP = dP*x(1);
rP = [cumsum(dPP,'reverse')];
rW = linspace(max(rP)-dPP(1)/2,0,length(cWW));
end

function [lambda, AspectRatio,WingArea] = Test(dP,lW,J,CL)
global BodyRadius
Aw = (lW.*dP);
WingArea = sum(Aw)*2 + BodyRadius*lW(end)*2;
Ap = pi.*dP.^2/4;
WingSpan = sum(dP)*2+2*BodyRadius-dP(1);
MAC = WingArea./WingSpan;
AspectRatio = WingSpan./MAC;
Ap(Ap==0) = 1e-4;
Aw(Aw==0) = 1e-4;
r = zeros(size(dP));
r(end) = dP(end)/2+BodyRadius;
for i = (length(r)-1):-1:1
    r(i) = r(i+1)+dP(i+1)/2+dP(i)/2;
end
B = J\[r.*(Aw).*CL./(Ap);r];
B= [B,-B];
lambda = eig(B*B');
end

function c = cost(x)
[dP,cW] = coefs2var(x);
global CL
global Jrp
[lambda, ~, ~] = Test(dP,cW,Jrp,CL);
c = (min(lambda)/max(lambda)-0.065).^2;
end

function [c,ceq] = mycon(x)
global minWingCord
[dP,cW,cWW] = coefs2var(x);

c = zeros(length(cWW)-1,1);
for i = 1:(length(cWW)-1)
    c(i) = cWW(i)-cWW(i+1);
end
c = [c;-[dP,cWW-minWingCord]'];
global CL
global Jrp
global AspectRatioReq
global WAReq
[~, AR,WA] = Test(dP,cW,Jrp,CL);
ceq = [0,0];
ceq(1) = AspectRatioReq-AR;
ceq(2) = WAReq-WA;
end