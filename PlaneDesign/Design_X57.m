function [Wing,VOP,Plane,Trim] = Design_X57(plot_flag)
% Whis is the main function for the basic design of experimental model
% The model is based on X-57 experimental aircraft
%
% The function has no inputs
%
% The function has the following outputs:
% Wing:     Structure of wing parameters
% VOP:      Structure of wing parameters
% Plane:    Structure of plane parameters
% Trim:     Structure of the trim configuration parameters


X_57_parameters;

dP_w = [dP_w,dP_w(end:-1:1)];
cW_w = [cW_w,cW_w(end:-1:1)];
cWW_w = [cWW_w,cWW_w(end:-1:1)];
rP_w = [rP_w,rP_w(end:-1:1)];
rW_w = [rW_w,rW_w(end:-1:1)];

dP_v = [dP_v,dP_v(end:-1:1)];
cW_v = [cW_v,cW_v(end:-1:1)];
cWW_v = [cWW_v,cWW_v(end:-1:1)];
rP_v = [rP_v,rP_v(end:-1:1)];
rW_v = [rW_v,rW_v(end:-1:1)];

WingVopPosition;

dP_w = dP_w(1:length(dP_w)/2);
cW_w = cW_w(1:length(cW_w)/2);
cWW_w = cWW_w(1:length(cWW_w)/2);
rP_w = rP_w(1:length(rP_w)/2);
rW_w = rW_w(1:length(rW_w)/2);

dP_v = dP_v(1:length(dP_v)/2);
cW_v = cW_v(1:length(cW_v)/2);
cWW_v = cWW_v(1:length(cWW_v)/2);
rP_v = rP_v(1:length(rP_v)/2);
rW_v = rW_v(1:length(rW_v)/2);


Wing.dP = dP_w;         % propeller diameter (1 is on the wing edge, last is nex to the fuselage) [m]
Wing.cW = cW_w;         % wing MAC for section behind the  corresponding propeller [m]
Wing.A = A_w;           % Wing area [m]
Wing.AR = AR_w;         % Wing aspect ratio [~]
Wing.dx = rx_w;         % Ofset of wing lift from CG in x direction [m]
Wing.dz = rz_w;         % Ofset of thrust from CG in z direction [m]
Wing.CL = CL_cruise;    % Coefficient of lift for trim [~]
Wing.rWW = rW_w;
Wing.cWW = cWW_w;
  
VOP.dP = dP_v;          % propeller diameter (1 is on the vop edge, last is nex to the fuselage) [m]
VOP.cW = cW_v;          % vop MAC for section behind the  corresponding propeller [m]
VOP.A = A_v;            % VOP area [m]
VOP.AR = AR_v;          % VOP aspect ratio [~]
VOP.dx = rx_v;          % Ofset of vop lift from CG in x direction [m]
VOP.dz = rz_v;          % Ofset of thrust from CG in z direction [m]
VOP.CL = CL_cruise;     % Coefficient of lift for trim [~]
VOP.rWW = rW_v;
VOP.cWW = cWW_v;


Plane.MTOW = MTOW;       % Maximal take-off weight [kg]
Plane.J = J;             % Intertia matrix [kg*m^2]
Plane.CD = CD_cruise;    % Coefficient of drag for trim [~]

Trim.v_cruise = v_cruise-v_diff;    % trimmed cruise speed [m/s]
Trim.WingThrust = T_w;              % trimmed mean thrust for wing engines [N]
Trim.VOPThrust = T_v;               % trimmed mean thrust for vop engines  [N]
Trim.Lift = L_req;                  % trimmed lift [N]
Trim.Drag = drag_cruise;            % trimmed drag [N]

if exist('plot_flag','var')
    if plot_flag == 1
        PlotWing(dP_w,cW_w,cWW_w,rP_w,rW_w,c1_w,BR_w,rx_w)
        PlotWing(dP_v,cW_v,cWW_v,rP_v,rW_v,c1_v,BR_v,rx_v)
    end
end
end