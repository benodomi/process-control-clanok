%% Mission parameters
A_all = A_w + A_v;
global v_cruise drag_cruise L_req 
L_req = MTOW * g;
v_cruise = sqrt(L_req*2/(rho*CL_cruise*(A_all)));
CD_cruise = CL_cruise/15;
drag_cruise  = 0.5*rho*(A_all)*CD_cruise*v_cruise^2;
x0 = [-1,-1,1,1,drag_cruise/A_all*A_w/length(dP_w),drag_cruise/A_all*A_v/length(dP_v),0]; %rx_wing,rz_wing, rx_vop,rz_vop,T_wing, T_vop, v_diff
options = optimoptions('fmincon','Display','none','Algorithm','sqp','MaxFunctionEvaluations',1e6);
x = fmincon(@cost,x0,[],[],[],[],[],[],@mycon,options);
%[lambda, Thrust,Lift,M_y] = Test(x(1),x(2),x(3),x(4),x(5),x(6),x(7));
rx_w = x(1);rz_w = x(2); rx_v = x(3); rz_v = x(4); T_w = x(5); T_v = x(6); v_diff= x(7);
function [lambda, Thrust,Lift,M_y] = Test(x_w,z_w,x_v,z_v,T_w,T_v,v)
global dP_w cW_w dP_v cW_v rho v_cruise CL_cruise
v_inf = v_cruise-v;
Aw_w = (cW_w.*dP_w);
Ap_w = pi.*dP_w.^2/4;
Aw_v = (cW_v.*dP_v);
Ap_v = pi.*dP_v.^2/4;

x_w = ones(size(Aw_w))*x_w;
x_v = ones(size(Aw_v))*x_v;
z_w = ones(size(Aw_w))*z_w;
z_v = ones(size(Aw_v))*z_v;
T_w = ones(size(Aw_w))*T_w;
T_v = ones(size(Aw_v))*T_v;


ve_f = @(T,Ap) sqrt(2.*T./(rho.*Ap)+v_inf.^2);             % induced airspeed based on thrust: T = 0.5*rho*Ap*(ve^2-v0^2)
F_xz_f = @(T,Aw,Ap) [T;0.5*rho.*Aw.*(ve_f(T,Ap)).^2*CL_cruise];    % Thrust and Lift L: L = 0.5*rho*Aw*ve^2
F_wing = F_xz_f(T_w,Aw_w,Ap_w);
F_vop = F_xz_f(T_v,Aw_v,Ap_v);

Thrust = sum(F_wing(1,:)) + sum(F_vop(1,:));
Lift = sum(F_wing(2,:)) + sum(F_vop(2,:));
M_y =  +z_w*F_wing(1,:)' +z_v*F_vop(1,:)' +x_w*F_wing(2,:)' +x_v*F_vop(2,:)';
global MTOW J
ratio = [Aw_w,Aw_v]./[Ap_w,Ap_v].*CL_cruise;
B = [ones(size(ratio))/MTOW;-ratio/MTOW;(ratio.*[x_w,x_v]+[z_w,z_v])/(J(2,2))];
lambda = eig(B*B');
end


function c=cost(x)
[lambda, ~,~,~] = Test(x(1),x(2),x(3),x(4),x(5),x(6),x(7));
c = (min(lambda)/max(lambda)-1).^2 + (x(5)-x(6))^2;
end

function [c,ceq] = mycon(x)
global drag_cruise L_req
[~, Thrust,Lift,M_y] = Test(x(1),x(2),x(3),x(4),x(5),x(6),x(7));
c = [-x(1)+0.87,-x(2),x(3),x(4),-x(5),-x(6)];
%c = [x(1)+0.68,x(2),-x(3),-x(4),-x(5),-x(6)];

ceq = [Thrust-drag_cruise;Lift-L_req;M_y];
end