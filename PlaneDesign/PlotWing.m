function PlotWing(dP_out,cW_out,cWW2,rP,rW,c1,BodyRadius,dx)

WleadingEdge = cWW2*c1+mean(cWW2);
WtrailingEdge = -cWW2*(1-c1)+mean(cWW2);
WleadingEdgeProp = cW_out*c1 + mean(cWW2);
WtrailingEdgeProp = -cW_out*(1-c1) + mean(cWW2);

plot(BodyRadius+rW,WleadingEdge-max(WleadingEdge)+dx,'b')
hold on
plot(-BodyRadius-rW,WleadingEdge-max(WleadingEdge)+dx,'b')
plot([-BodyRadius,BodyRadius],[WleadingEdge(end),WleadingEdge(end)]-max(WleadingEdge)+dx,'b')

plot(BodyRadius+rW,WtrailingEdge-max(WleadingEdge)+dx,'b')
hold on
plot(-BodyRadius-rW,WtrailingEdge-max(WleadingEdge)+dx,'b')
plot([-BodyRadius,BodyRadius],[WtrailingEdge(end),WtrailingEdge(end)]-max(WleadingEdge)+dx,'b')

eng_possX = WleadingEdgeProp + dP_out/2-max(WleadingEdge);
eng_possY = BodyRadius+rP-dP_out/2;
for i = 1:length(rP)
    plot(BodyRadius+rP(i)+[-dP_out(i),0],WleadingEdgeProp(i) + [dP_out(i),dP_out(i)]/2-max(WleadingEdge)+dx,'r')
end
for i = 1:length(rP)
    plot(-BodyRadius-rP(i)-[-dP_out(i),0],WleadingEdgeProp(i) + [dP_out(i),dP_out(i)]/2-max(WleadingEdge)+dx,'r')
end
axis equal
plot([max(rW)+BodyRadius,max(rW)+BodyRadius],[WtrailingEdge(1),WleadingEdge(1)]-max(WleadingEdge)+dx,'b')
plot([-max(rW)-BodyRadius,-max(rW)-BodyRadius],[WtrailingEdge(1),WleadingEdge(1)]-max(WleadingEdge)+dx,'b')

end